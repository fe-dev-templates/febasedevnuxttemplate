var Promise = require('es6-promise').Promise;
import axios from 'axios';

const User = async () => {
	const res =  await axios.get('https://randomuser.me/api');
	const user = res.data.results[0];

	console.log(user)

	const template = `
		User is ready, please open your console and see the return data
		<br />
		<img src="${user.picture.large}" />
		<h2>${user.name.first} ${user.name.last}</h2>
		<ul>
			<li>${user.email}</li>
			<li>${user.phone}</li>
			<li>${user.location.city}</li>
		</ul>
	`;

	return template;
}

export default User;