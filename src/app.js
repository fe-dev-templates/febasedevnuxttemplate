import Header from './components/Header';
import User from './components/User';

import './scss/style.scss';

console.log('Hello from app.js');

const app = async () => {
	document.getElementById('header').innerHTML = Header();
	document.getElementById('user').innerHTML = await User();
}

// Init app
app();